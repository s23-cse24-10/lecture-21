#include <iostream>
#include "board.h"
#include "tictactoe.h"
using namespace std;

Vec simpleAI(Board board, SquareState player) {
    for (int i = 0; i < board.getSize(); i++) {
        for (int j = 0; j < board.getSize(); j++) {
            if (board.getGrid(i, j) == EMPTY) {
                return Vec(i, j);
            }
        }
    }

    return Vec(0, 0);
}

Vec human(Board board, SquareState player) {
    if (player == PLAYER1) {
        int x, y;
        bool success = false;
        while (!success) {
            cout << "Player 1:";
            cin >> x >> y;
            success = board.updateGrid(x, y, player);
        }
        return Vec(x, y);
    } else {
        int x, y;
        bool success = false;
        while (!success) {
            cout << "Player 2:";
            cin >> x >> y;
            success = board.updateGrid(x, y, player);
        }
        return Vec(x, y);
    }
}

int main(int argc, char* argv[]) {

    // ./app
    // ./app 3
    // ./app h h
    // ./app 5 s s
    int size = 3;
    Vec (*funcPtr1)(Board, SquareState) = human;
    Vec (*funcPtr2)(Board, SquareState) = human;

    if (argc == 2) {
        size = stoi(argv[1]);
    }

    if (argc == 3) {
        char player1 = argv[1][0];
        char player2 = argv[2][0];

        if (player1 == 's') {
            funcPtr1 = simpleAI;
        }
        if (player2 == 's') {
            funcPtr2 = simpleAI;
        }
    }

    if (argc == 4) {
        size = stoi(argv[1]);
        char player1 = argv[2][0];
        char player2 = argv[3][0];

        if (player1 == 's') {
            funcPtr1 = simpleAI;
        }
        if (player2 == 's') {
            funcPtr2 = simpleAI;
        }
    }

    TicTacToe game(size);
    game.setPlayer1(funcPtr1);
    game.setPlayer2(funcPtr2);
    game.play();

    return 0;
}